package com.example.apitechudb.services;


import com.example.apitechudb.models.ProductModel;
import com.example.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServices {


    @Autowired
    ProductRepository productRepository;


    public List<ProductModel> findAll() {

        System.out.println();

        return this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product){
        System.out.println("add ProductServices");
        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById ProductService");
        return this.productRepository.findById(id);
    }

    public ProductModel setProduct(ProductModel product){
        System.out.println("setProduct productService");
        return this.productRepository.insert(product);
    }
}
