package com.example.apitechudb.controller;

import com.example.apitechudb.models.ProductModel;
import com.example.apitechudb.services.ProductServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductServices productServices;

    @GetMapping("/products")
        public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productServices.findAll()
                , HttpStatus.OK
        );
    }


    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("Alta con id " + product.getId());
        System.out.println("Alta con des " + product.getDesc());
        System.out.println("Alta con precio " + product.getPrice());

        ProductModel result;
        return new ResponseEntity<>(
              this.productServices.add(product),
                HttpStatus.CREATED
                );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id a buscar " + id);

        Optional<ProductModel> result = this.productServices.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable String id, @RequestBody ProductModel product){

        System.out.println("insertProduct");
        System.out.println("crear producto con id " + id);
        System.out.println("crear producto con Desc " + product.getId());
        System.out.println("crear producto con DescResponseEntity " + product.getDesc());
        System.out.println("crear producto con Price " + product.getPrice());

        Optional<ProductModel> result = this.productServices.findById(id);

        return new ResponseEntity<>(

                   result.isPresent() ?  "Registro ya existente" : this.productServices.setProduct(product),
                   result.isPresent() ? HttpStatus.BAD_REQUEST : HttpStatus.OK

                );
    }



}
